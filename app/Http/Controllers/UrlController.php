<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UrlResource;
use App\Url;
use Response;
use Validator;


use Illuminate\Foundation\Http\FormRequest;

class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url'    => 'url'
        ]);

        if ($validator->fails()) {
            return \Response::json(['error' => 'invalid url'], 404);
        }
        else{
            $url = Url::create([
                    'url' => "$request->url",
                    'code_short_url' => str_random(8)
            ]);
            return new UrlResource($url);
        }
    }


    public function massive(Request $request){
        $input_urls = $request->url;
        foreach ($input_urls as $url)
        {
            $code = Url::create([
                'url' => $url,
                'code_short_url' => str_random(8)
            ])->code_short_url;

            $urls[] = url('/',$code);
        }
        return Response::json(['short_url'=>$urls]);
    }

     
}
