<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UrlResource;
use App\Url;
use Illuminate\Support\Facades\Redirect;


class WelcomeController extends Controller
{
     
    public function getCode($id)
    {
       $url = Url::where('code_short_url',$id)->first(); 
       return redirect()->away((is_null(parse_url($url->url, PHP_URL_HOST)) ? '//' : '').$url->url);
    } 
}
