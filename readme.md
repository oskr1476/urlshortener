## Proyecto desarrollado en PHP usando el framework Laravel

API para acortar urls. 

## Requisitos

- PHP 7.1
- Laravel 5.6
- Composer
- mysql


## Instrucciones

Asegure de tener una base de datos llamada urlshortener

En la carpeta del proyecto, ejecuta los siguientes comandos en la terminal. 

- composer install
- php artisan migrate

Modifique de ser necesario las credenciales para su base de datos en el archivo .env localizado en la ra�z del proyecto.

> DB_CONNECTION=mysql
>
> DB_HOST=127.0.0.1
>
> DB_PORT=3306
>
> DB_DATABASE=urlshortener
>
> DB_USERNAME=root
>
> DB_PASSWORD=

Ejecute el servidor con el siguiente comando: 

- php artisan serve

## Uso

La documentaci�n de los endpoints se encuentra en el siguiente enlace (https://documenter.getpostman.com/view/1779112/RWTptx5i).

1.- Para la tarea 1, se debe consultar el endpoint task 1

2.- Para la tarea 2, se debe consultar el endpoint task 2

3.- Para la tarea 3, se debe pegar la url acortada en algun navegador.
